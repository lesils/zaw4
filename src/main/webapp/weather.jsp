<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Pogoda</title>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
      <script>
          $( document ).ready(function() {
              $(".check").click(function(){
                  var city = $('#city').find(":selected").text();
                  $.ajax({ url: "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&APPID=7481a824faa1682e684728eefe5e24a0&units=metric",
                     success: function(result){
                         console.log(result);
                          $("#zachmurzenie").text(result.clouds.all);
                          $("#temperatura").text((result.main.temp));
                          $("#cisnienie").text(result.main.pressure);
                          $("#szybkosc_wiatru").text(result.wind.speed);
                      }
                  });
              });
          });
      </script>
  </head>
  <body>
  <h2>Sprawdz pogodę w wybranym wielkim miescie Polski</h2>
  <select id="city">
    <option value="gdynia">Gdynia</option>
    <option value="gdańsk">Gdańsk</option>
    <option value="kraków">Kraków</option>
    <option value="wrocław">Wrocław</option>
    <option value="poznań">Poznań</option>
    <option value="łódź">Łódź</option>
    <option value="warszawa">Warsawa</option>
  </select>
  <button class="check">check</button><br>
  <img id="icon">
<div id ="pogoda">
 
  Poziom zachmurzenia: <label id="zachmurzenie"></label><br>
Temperatura <label id="temperatura"></label><br>
Ciśnienie <label id="cisnienie"></label><br>
Moc wiatru <label id="szybkosc_wiatru"></label><br>
 
</div>
  </body>
</html>